//Теоретичні питання
// 1.Опишіть своїми словами що таке Document Object Model (DOM)
// DOM це об'єктна модель документа, тобто DOM бачить всі елементи на сторінці як об'єкти. Браузеру потрібен DOM для того, щоб за допомогою Javascript можна було швидко вносити певні зміни у веб-документ. Такими змінами можуть бути додавання нових елементів на сторінку, пошук потрібних елементів ну і багато іншого.
//
//2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//innerHTML повертає як сам текст, так і теги, а innerText повертає лише текст, як рядок, якій знаходиться у HTML (самі ж теги не повертає).
//
//3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//До елемента на сторінці за допомогою JS можна звернутись різними способами:
//1)document.getElementById() - це метод, який дозволяє отримати елемент за його унікальним ідентифікатором (id)
//2)document.getElementsByClassName() - метод, який повертає елемент, який має певний клас (записаний у дужках)
//3)document.getElementsByTagName() - метод, який шукає елемент з цим тегом (тег записується в дужках)
//4)document.querySelectorAll() - цей метод повертає всі елементи, які задовольняють даний селектор
//5)document.querySelector() - повертає перший елемент, що відповідає даному селектору
//Який спосіб є кращим - це залежить від того, що потрібно отримати.Якщо є id, то використання getElementById є найшвидшим варіантом. В інших же випадках, коли потрібно вибрати елементи за класами або іншими атрибутами, варто розглянути querySelector або querySelectorAll.
//

//1.Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphsColor = document.querySelectorAll("p");

function setParagraphBackgroundColor(paragraph) {
    paragraph.style.backgroundColor = "#ff0000";
}

paragraphsColor.forEach(function(paragraph) {
    setParagraphBackgroundColor(paragraph);
});

//2.Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const optionsID = document.getElementById("optionsList");
console.log(optionsID);

const parentElement = document.getElementById("optionsList").parentNode;
console.log(parentElement);

const childElement = document.getElementById("optionsList").childNodes;
console.log(childElement);

//3.Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph
const idElement = document.getElementById("testParagraph");
idElement.innerText = "This is a paragraph";

//4.Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.
const headerClass = document.querySelector(".main-header");

const nestedElements = headerClass.querySelectorAll("*");

nestedElements.forEach((element) => {
    element.classList.add("nav-item");
});

console.log(headerClass);

//5.Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const deleteClass = document.querySelectorAll(".section-title");

deleteClass.forEach((element) => {
    element.classList.remove("section-title");
});

console.log(deleteClass);


